import { MainScene } from './scenes/MainScene';
import { Preloader } from './scenes/Preloader';

const config: GameConfig = {
    title: 'Phaser - Starter',
    url: 'https://github.com/digitsensitive/phaser3-typescript',
    version: '1.0',
    width: 1280,
    height: 720,
    type: Phaser.WEBGL,
    parent: 'game',
    scene: [Preloader, MainScene],
    input: {
        keyboard: true,
        mouse: true,
        touch: false,
        gamepad: false
    },
    dom: {
        createContainer: true
    },
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    backgroundColor: '#000000'
};

export class Game extends Phaser.Game {
    constructor(config: GameConfig) {
        super(config);
    }
}

window.onload = () => {
    const game = new Game(config);
};
