export class Preloader extends Phaser.Scene {
    private continueButton: Phaser.GameObjects.Image;
    private continueButtonFadeAnimation: Phaser.Tweens.Tween;
    private sceneGroup: Phaser.GameObjects.Group;

    constructor() {
        super({
            key: 'Preloader'
        });
    }

    public preload() {
        this.load.spritesheet('continueAnimation', 'assets/images/continueAnimation.png', { frameWidth: 325, frameHeight: 45, endFrame: 53 });
        this.load.image('continueButton', 'assets/images/continueButton.png');
    }

    public create() {
        this.sceneGroup = new Phaser.GameObjects.Group(this);
        this.anims.create({ key: 'everything', frames: this.anims.generateFrameNames('continueAnimation') });
        const sprite = this.add
            .sprite(640, 300, 'continueAnimation')
            .setScale(2)
            .play('everything');
        sprite.on('animationcomplete', this.animComplete, this);

        this.continueButton = this.add.image(640, 420, 'continueButton');
        this.continueButton.alpha = 0;
        this.continueButton.on('pointerover', (event) => {
            this.cameras.main.zoomTo(1.8, 1000, 'Power2', true);
        });
        this.continueButton.on('pointerout', (event) => {
            this.cameras.main.zoomTo(1, 2000, 'Elastic', true);
        });
        this.continueButton.on('pointerdown', (event) => {
            this.cameras.main.shake(1000, 0.06, true);
            this.time.addEvent({
                delay: 1000,
                callback: () => {
                    this.scene.transition({
                        target: 'MainScene',
                        duration: 2000,
                        moveBelow: true,
                        onUpdate: this.transitionOut,
                        data: { x: 400, y: 300 }
                    });
                }
            });
        });

        this.createAnimations();
    }

    private transitionOut(progress: number) {
        this.children
            .getAll()
            .filter((item) => (item as any).x != undefined)
            .forEach((item) => ((item as any).x = 640 + 1500 * progress));
    }

    private createAnimations() {
        this.continueButtonFadeAnimation = this.tweens.add({
            targets: this.continueButton,
            alpha: 1,
            ease: 'Power1',
            duration: 3000,
            paused: true
        });
    }

    private animComplete() {
        this.continueButtonFadeAnimation.play(true);
        this.continueButton.setInteractive();
    }
}
