/**
 * @author       Digitsensitive <digit.sensitivee@gmail.com>
 * @copyright    2018 Digitsensitive
 * @license      Digitsensitive
 */
import { TweenMax } from 'gsap/TweenMax';
import WebFont = require('webfontloader');
export class MainScene extends Phaser.Scene {
    private phaserSprite: Phaser.GameObjects.Sprite;
    private debugButton: Phaser.GameObjects.Sprite;
    private cowboyActionButton: Phaser.GameObjects.Sprite;
    private counter: number;
    private cowboy: any;
    private firstGropuOfSymbols: Phaser.GameObjects.Group;
    private secondGropuOfSymbols: Phaser.GameObjects.Group;
    private thirdGropuOfSymbols: Phaser.GameObjects.Group;
    private fourthGropuOfSymbols: Phaser.GameObjects.Group;
    private fifthGropuOfSymbols: Phaser.GameObjects.Group;
    private sixGropuOfSymbols: Phaser.GameObjects.Group;
    private sevenGropuOfSymbols: Phaser.GameObjects.Group;
    private eightGropuOfSymbols: Phaser.GameObjects.Group;
    private gambleAnimation: Phaser.GameObjects.Sprite;
    private fpsText: Phaser.GameObjects.Text;
    private blitter: Phaser.GameObjects.Blitter;
    private posX = 0;
    private direction = 1;
    private index = 0;
    private text: Phaser.GameObjects.Text;
    private mesh: Phaser.GameObjects.Mesh;
    private quad: any;
    private textForAnimation: Phaser.GameObjects.Text;
    private textForAnimationWebFont1: Phaser.GameObjects.Text;
    private textForAnimationWebFont2: Phaser.GameObjects.Text;
    private textForAnimationBitmapFont: Phaser.GameObjects.BitmapText;
    private textTween: Phaser.Tweens.Tween;

    constructor() {
        super({
            key: 'MainScene',
            pack: {
                files: [{ type: 'scenePlugin', key: 'SpineWebGLPlugin', url: 'plugins/spineWEBGL.js', sceneKey: 'spine' }]
            }
        } as any);

        this.counter = 0;
        this.quad = {
            topLeftX: -200,
            topLeftY: -200,
            topRightX: 200,
            topRightY: -200,
            bottomLeftX: -200,
            bottomLeftY: 200,
            bottomRightX: 200,
            bottomRightY: 200
        };
    }

    public init() {
        const element = document.createElement('style');

        document.head.appendChild(element);

        const sheet = element.sheet;

        let styles = '@font-face { font-family: "troika"; src: url("assets/fonts/troika.otf") format("opentype"); }\n';

        (sheet as any).insertRule(styles, 0);

        styles = '@font-face { font-family: "Caroni"; src: url("assets/fonts/caroni.otf") format("opentype"); }';

        (sheet as any).insertRule(styles, 0);
    }

    public update() {
        this.fpsText.setText(`FPS: ${this.game.loop.actualFps}`);
        this.textForAnimation.setText(this.textTween.getValue().toFixed(2));
        this.textForAnimationWebFont1.setText(this.textTween.getValue().toFixed(3));
        this.textForAnimationWebFont2.setText(this.textTween.getValue().toFixed(1));
        this.textForAnimationBitmapFont.setText(this.textTween.getValue().toFixed(1));
    }

    public preload(): void {
        this.load.html('loginForm', 'assets/html/loginForm.html');
        this.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js');
        this.load.image('logo', 'assets/images/phaser.png');
        this.load.image('action', 'assets/images/action.png');
        this.load.image('spark', 'assets/images/blue.png');
        this.load.spritesheet('balls', 'assets/images/balls.png', { frameWidth: 17, frameHeight: 17 });
        this.load.svg('debug', 'assets/images/svg/debug.svg');
        this.load.multiatlas('gamble', 'assets/atlases/gamble.json');
        this.load.atlas('symbols1', 'assets/atlases/symbols2.png', 'assets/atlases/symbols2.json');
        this.load.atlas('symbols2', 'assets/atlases/symbols3.png', 'assets/atlases/symbols3.json');
        this.load.atlas('symbols3', 'assets/atlases/symbols4.png', 'assets/atlases/symbols4.json');
        this.load.atlas('flares', 'assets/atlases/flares.png', 'assets/atlases/flares.json');
        this.load.bitmapFont('woodenFOnt', 'assets/fonts/woodenFont.png', 'assets/fonts/woodenFont.xml');

        (this.load as any).spine('cowboy', 'assets/spine/cowboy.json', 'assets/spine/cowboy.atlas');
    }

    public create(): void {
        this.phaserSprite = this.add.sprite(500, 300, 'logo');
        this.createCowboy();
        this.createCowboyButtons();
        this.createPhaserLogoAnimation();
        this.createSymbolsStripAnimation();
        this.createTweenTest();
        this.createMeshAnimCowboy();
        this.createParticlesEmiters();
        this.createTextAnimation();
        this.createLoginForm();
        this.fpsText = this.add
            .text(200, 400, 'Fps')
            .setFontFamily('Arial')
            .setFontSize(64)
            .setColor('#FFAABB');
        this.animPhaserImage();
        this.events.on(
            'transitionstart',
            (fromScene, duration) => {
                this.children
                    .getAll()
                    .filter((item) => (item as any).alpha)
                    .forEach((item) => ((item as any).alpha = 0));
                this.tweens.add({
                    targets: this.children.getAll().filter((item) => (item as any).x),
                    alpha: 1,
                    duration
                });
            },
            this
        );
    }

    private createLoginForm() {
        const element = (this.add.dom(200, 0, undefined) as any).createFromCache('loginForm');

        // element.setPerspective(1000);

        element.addListener('click');

        element.on('click', (event) => {
            if (event.target.name === 'loginButton') {
                const inputUsername = element.getChildByName('username');

                //  Have they entered anything?
                if (inputUsername.value !== '') {
                    //  Turn off the click events
                    element.removeListener('click');

                    this.textTween.totalTargets = +event.target.name;
                    this.textTween.play(true);

                    this.tweens.add({
                        targets: element,
                        scaleX: 2,
                        scaleY: 2,
                        y: 1000,
                        duration: 3000,
                        ease: 'Power3',
                        onComplete() {
                            element.setVisible(false);
                        }
                    });
                } else {
                    //  Flash the prompt
                    // this.scene.tweens.add({ targets: text, alpha: 0.1, duration: 200, ease: 'Power3', yoyo: true });
                }
            }
        });

        this.tweens.add({
            targets: element,
            y: 500,
            duration: 3000,
            ease: 'Power3'
        });
    }

    private createTextAnimation() {
        this.textForAnimation = this.add.text(300, 80, '0', { font: '30px Courier', fill: '#00ff00' });
        this.textForAnimation.setOrigin(0.5);

        //  A 'Counter' tween is a special type of tween which doesn't have a target.
        //  Instead it allows you to tween between 2 numeric values. The default values
        //  are 0 to 1, but can be set to anything. You can use the tweened value via
        //  `tween.getValue()` for the duration of the tween.

        this.textTween = this.tweens.addCounter({
            from: 0,
            to: 500,
            duration: 80000,
            yoyo: true,
            repeat: -1,
            paused: true
        });

        this.textForAnimationBitmapFont = this.add.bitmapText(300, 580, 'woodenFOnt', 'test', 30);
        // this.textForAnimationBitmapFont.setAngle(30);
        this.textForAnimationBitmapFont.setOrigin(0.2, 0.2);
        // this.textForAnimationBitmapFont.setCenterAlign();

        WebFont.load({
            custom: {
                families: ['troika', 'Caroni']
            },
            active: () => {
                this.textForAnimationWebFont1 = this.add
                    .text(300, 180, 'Test', { fontFamily: 'troika', fontSize: 30, color: '#ff0000' })
                    .setShadow(2, 2, '#333333', 2, false, true);
                this.textForAnimationWebFont1.setOrigin(0.5);

                this.textForAnimationWebFont2 = this.add.text(300, 280, 'Test', {
                    fontFamily: 'Caroni',
                    fontSize: 30,
                    color: '#5656ee'
                });
                this.textForAnimationWebFont2.setOrigin(0.5);

                [this.textForAnimation, this.textForAnimationWebFont1, this.textForAnimationWebFont2, this.textForAnimationBitmapFont].forEach((item) =>
                    this.tweens.add({
                        targets: item,
                        scaleX: 4,
                        scaleY: 4,
                        // x: item.x + 200,
                        // y: item.y - 30,
                        duration: 20000,
                        yoyo: true,
                        repeat: -1
                    })
                );
            }
        });
    }

    private createParticlesEmiters() {
        const particles = this.add.particles('spark');

        particles.createEmitter({
            x: 1250,
            y: 30,
            angle: { min: 140, max: 180 },
            speed: 400,
            gravityY: 200,
            lifespan: { min: 1000, max: 2000 },
            blendMode: 1
        });

        const emiter = particles.createEmitter({
            x: 30,
            y: 30,
            angle: { min: 340, max: 400 },
            speed: 300,
            gravityY: 800,
            lifespan: { min: 1000, max: 2000 },
            blendMode: 1,
            tint: 0xff3322
        });

        const origin = this.phaserSprite.getTopLeft();
        let pixel;
        let positionXEmiter;
        let positionYEmiter;
        const logoSource = {
            getRandomPoint: (vec) => {
                do {
                    positionXEmiter = Phaser.Math.Between(0, this.phaserSprite.width);
                    positionYEmiter = Phaser.Math.Between(0, this.phaserSprite.height);
                    pixel = this.textures.getPixel(positionXEmiter, positionYEmiter, 'logo', 0);
                } while (pixel && pixel.alpha < 255);

                return vec.setTo(positionXEmiter + origin.x, positionYEmiter + origin.y);
            }
        };

        const particles2 = this.add.particles('flares');

        particles2.createEmitter({
            x: 0,
            y: 0,
            lifespan: 1000,
            gravityY: 10,
            scale: { start: 0, end: 0.25, ease: 'Quad.easeOut' },
            alpha: { start: 1, end: 0, ease: 'Quad.easeIn' },
            blendMode: 1,
            emitZone: { type: 'random', source: logoSource }
        });
    }

    private createMeshAnimCowboy() {
        this.mesh = this.make.mesh({
            key: 'logo',
            x: 400,
            y: 300,
            vertices: [
                this.quad.topLeftX,
                this.quad.topLeftY,
                this.quad.bottomLeftX,
                this.quad.bottomLeftY,
                this.quad.bottomRightX,
                this.quad.bottomRightY,

                this.quad.topLeftX,
                this.quad.topLeftY,
                this.quad.bottomRightX,
                this.quad.bottomRightY,
                this.quad.topRightX,
                this.quad.topRightY
            ],
            uv: [0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0]
        });

        this.tweenQuad();
    }

    private tweenQuad() {
        //  Randomise the coords a little

        const tlX = -200 + Phaser.Math.Between(-90, 90);
        const tlY = -200 + Phaser.Math.Between(-90, 90);

        const trX = 200 + Phaser.Math.Between(-90, 90);
        const trY = -200 + Phaser.Math.Between(-90, 90);

        const blX = -200 + Phaser.Math.Between(-90, 90);
        const blY = 200 + Phaser.Math.Between(-90, 90);

        const brX = 200 + Phaser.Math.Between(-90, 90);
        const brY = 200 + Phaser.Math.Between(-90, 90);

        TweenMax.to(this.quad, 1.5, {
            topLeftX: tlX,
            topLeftY: tlY,

            topRightX: trX,
            topRightY: trY,

            bottomLeftX: blX,
            bottomLeftY: blY,

            bottomRightX: brX,
            bottomRightY: brY,

            ease: 'Elastic.easeOut',

            onUpdate: () => {
                const verts = this.mesh.vertices;

                verts[0] = this.quad.topLeftX;
                verts[1] = this.quad.topLeftY;
                verts[6] = this.quad.topLeftX;
                verts[7] = this.quad.topLeftY;

                verts[10] = this.quad.topRightX;
                verts[11] = this.quad.topRightY;

                verts[2] = this.quad.bottomLeftX;
                verts[3] = this.quad.bottomLeftY;

                verts[4] = this.quad.bottomRightX;
                verts[5] = this.quad.bottomRightY;
                verts[8] = this.quad.bottomRightX;
                verts[9] = this.quad.bottomRightY;
            },

            onComplete: () => this.tweenQuad()
        });
    }

    private createTweenTest() {
        console.log('create Tween test');
        this.blitter = this.add.blitter(1, 1, 'balls');
        this.text = this.add.text(10, 650, 'Test');
        this.time.addEvent({ delay: 10, callback: this.launchTweenTest, callbackScope: this, repeat: 10000 });
    }

    private launchTweenTest() {
        console.log('launchTweenTest');
        this.index++;

        if (this.posX >= 450) {
            this.direction = -1;
        } else if (this.posX < 180) {
            this.direction = 1;
        }

        this.posX += 2 * this.direction;

        this.text.setText(
            'Position: (' + this.posX + ', ' + 100 + ') \nActive Tweens: ' + (this.tweens as any)._active.length + '\nTotal Tweens created: ' + this.index
        );

        const bob = this.blitter.create(120, 300, Phaser.Math.Between(0, 5));
        this.tweens.add({
            targets: bob,
            x: this.input.activePointer.x,
            y: this.input.activePointer.y,
            duration: Phaser.Math.Between(500, 1500),
            ease: ['Power1', 'Elastic', 'Bounce.easeInOut', 'Cubic.easeInOut', 'Quad.easeOut'][Math.floor(Math.random() * 5)],
            // yoyo: true,
            onComplete() {
                bob.destroy();
            }
        });
    }

    private createSymbolsStripAnimation() {
        this.anims.create({
            key: 'symbol',
            frames: this.anims.generateFrameNames('symbols1', { prefix: 'chest_', end: 79, zeroPad: 3 }),
            repeat: -1,
            frameRate: 17
        });

        this.anims.create({
            key: 'symbol1',
            frames: this.anims.generateFrameNames('symbols1', { prefix: 'clam_', end: 49, zeroPad: 3 }),
            repeat: -1,
            frameRate: 17
        });

        this.anims.create({
            key: 'symbol2',
            frames: this.anims.generateFrameNames('symbols2', { prefix: 'mermaid_', end: 59, zeroPad: 3 }),
            repeat: -1,
            frameRate: 17
        });

        this.anims.create({
            key: 'symbol3',
            frames: this.anims.generateFrameNames('symbols2', { prefix: 'scatterReveal_', end: 12, zeroPad: 3 }),
            repeat: -1,
            frameRate: 17
        });

        this.anims.create({
            key: 'symbol4',
            frames: this.anims.generateFrameNames('symbols2', { prefix: 'scatterTrident_', end: 44, zeroPad: 3 }),
            repeat: -1,
            frameRate: 17
        });

        this.anims.create({
            key: 'symbol5',
            frames: this.anims.generateFrameNames('symbols3', { prefix: 'Q_', end: 19, zeroPad: 3 }),
            repeat: -1,
            frameRate: 17
        });

        this.anims.create({
            key: 'symbol6',
            frames: this.anims.generateFrameNames('symbols3', { prefix: 'seashell_', end: 99, zeroPad: 3 }),
            repeat: -1,
            frameRate: 17
        });

        this.createMovmentGroup();
    }

    private createMovmentGroup() {
        this.firstGropuOfSymbols = new Phaser.GameObjects.Group(this);
        this.secondGropuOfSymbols = new Phaser.GameObjects.Group(this);
        this.thirdGropuOfSymbols = new Phaser.GameObjects.Group(this);
        this.fourthGropuOfSymbols = new Phaser.GameObjects.Group(this);
        this.fifthGropuOfSymbols = new Phaser.GameObjects.Group(this);
        this.sixGropuOfSymbols = new Phaser.GameObjects.Group(this);
        this.sevenGropuOfSymbols = new Phaser.GameObjects.Group(this);
        this.eightGropuOfSymbols = new Phaser.GameObjects.Group(this);
        for (let i = 0; i < 10; i++) {
            this.firstGropuOfSymbols.add(this.add.sprite(1150, -80 - i * 160, 'seacreatures').play(`symbol${Math.ceil(Math.random() * 6)}`));
            this.secondGropuOfSymbols.add(this.add.sprite(1150, -80 - i * 160, 'seacreatures').play(`symbol${Math.ceil(Math.random() * 6)}`));
            this.thirdGropuOfSymbols.add(this.add.sprite(950, -80 - i * 160, 'seacreatures').play(`symbol${Math.ceil(Math.random() * 6)}`));
            this.fourthGropuOfSymbols.add(this.add.sprite(950, -80 - i * 160, 'seacreatures').play(`symbol${Math.ceil(Math.random() * 6)}`));
            this.fifthGropuOfSymbols.add(this.add.sprite(750, -80 - i * 160, 'seacreatures').play(`symbol${Math.ceil(Math.random() * 6)}`));
            this.sixGropuOfSymbols.add(this.add.sprite(750, -80 - i * 160, 'seacreatures').play(`symbol${Math.ceil(Math.random() * 6)}`));
            this.sevenGropuOfSymbols.add(this.add.sprite(550, -80 - i * 160, 'seacreatures').play(`symbol${Math.ceil(Math.random() * 6)}`));
            this.eightGropuOfSymbols.add(this.add.sprite(550, -80 - i * 160, 'seacreatures').play(`symbol${Math.ceil(Math.random() * 6)}`));
        }
        const animationTime = 2000;
        this.firstGropuOfSymbols.children
            .getArray()
            .filter((item) => (item as any).y != undefined)
            .forEach((item) =>
                this.tweens.add({
                    targets: item,
                    y: (item as any).y + 720 + 10 * 160,
                    duration: animationTime,
                    repeatDelay: (1600 / (720 + 22 * 160)) * animationTime,
                    repeat: -1
                })
            );
        this.secondGropuOfSymbols.children
            .getArray()
            .filter((item) => (item as any).y != undefined)
            .forEach((item) =>
                this.tweens.add({
                    targets: item,
                    y: (item as any).y + 720 + 10 * 160,
                    duration: animationTime,
                    delay: (1600 / (720 + 10 * 160)) * animationTime,
                    repeatDelay: (1600 / (720 + 22 * 160)) * animationTime,
                    repeat: -1
                })
            );
        this.thirdGropuOfSymbols.children
            .getArray()
            .filter((item) => (item as any).y != undefined)
            .forEach((item) =>
                this.tweens.add({
                    targets: item,
                    y: (item as any).y + 720 + 10 * 160,
                    duration: 3000,
                    repeatDelay: (1600 / (720 + 22 * 160)) * 3000,
                    repeat: -1
                })
            );
        this.fourthGropuOfSymbols.children
            .getArray()
            .filter((item) => (item as any).y != undefined)
            .forEach((item) =>
                this.tweens.add({
                    targets: item,
                    y: (item as any).y + 720 + 10 * 160,
                    duration: 3000,
                    delay: (1600 / (720 + 10 * 160)) * 3000,
                    repeatDelay: (1600 / (720 + 22 * 160)) * 3000,
                    repeat: -1
                })
            );
        this.fifthGropuOfSymbols.children
            .getArray()
            .filter((item) => (item as any).y != undefined)
            .forEach((item) =>
                this.tweens.add({
                    targets: item,
                    y: (item as any).y + 720 + 10 * 160,
                    duration: 1500,
                    repeatDelay: (1600 / (720 + 22 * 160)) * 1500,
                    repeat: -1
                })
            );
        this.sixGropuOfSymbols.children
            .getArray()
            .filter((item) => (item as any).y != undefined)
            .forEach((item) =>
                this.tweens.add({
                    targets: item,
                    y: (item as any).y + 720 + 10 * 160,
                    duration: 1500,
                    delay: (1600 / (720 + 10 * 160)) * 1500,
                    repeatDelay: (1600 / (720 + 22 * 160)) * 1500,
                    repeat: -1
                })
            );
        this.sevenGropuOfSymbols.children
            .getArray()
            .filter((item) => (item as any).y != undefined)
            .forEach((item) =>
                this.tweens.add({
                    targets: item,
                    y: (item as any).y + 720 + 10 * 160,
                    duration: 10000,
                    repeatDelay: (1600 / (720 + 22 * 160)) * 10000,
                    repeat: -1
                })
            );
        this.eightGropuOfSymbols.children
            .getArray()
            .filter((item) => (item as any).y != undefined)
            .forEach((item) =>
                this.tweens.add({
                    targets: item,
                    y: (item as any).y + 720 + 10 * 160,
                    duration: 10000,
                    delay: (1600 / (720 + 10 * 160)) * 10000,
                    repeatDelay: (1600 / (720 + 22 * 160)) * 10000,
                    repeat: -1
                })
            );
    }

    private createPhaserLogoAnimation() {
        this.anims.create({
            key: 'gambleAnimation',
            frames: this.anims.generateFrameNames('gamble', { start: 1, end: 30, zeroPad: 3, prefix: 'win_animation_' }),
            repeat: -1,
            repeatDelay: 5000
        });

        this.gambleAnimation = this.add
            .sprite(700, 300, 'gamble')
            .setScale(1.2)
            .play('gambleAnimation');
    }

    private createCowboy() {
        this.cowboy = (this.add as any).spine(120, 240, 'cowboy', 'harmonica_playing', true);
        this.cowboy.drawDebug = true;
        this.cowboy.setScale(0.5);
    }

    private createCowboyButtons() {
        this.debugButton = this.add.sprite(50, 660, 'debug');
        this.debugButton.setScale(0.5);
        this.debugButton.setInteractive();
        this.debugButton.on('pointerdown', (event) => {
            this.cowboy.drawDebug = !this.cowboy.drawDebug;
        });

        this.cowboyActionButton = this.add.sprite(150, 660, 'action');
        this.cowboyActionButton.setScale(0.15);
        this.cowboyActionButton.setInteractive();
        this.cowboyActionButton.on('pointerdown', (event) => {
            this.cowboy.play(Phaser.Utils.Array.GetRandom(this.cowboy.getAnimationList()), true);
        });
    }

    private animPhaserImage() {
        const newXOffset = Math.random() * 500;
        const newYOffset = Math.random() * 500;

        this.children.bringToTop(this.phaserSprite);
        this.children.bringToTop(this.gambleAnimation);
        this.tweens.add({
            targets: this.phaserSprite,
            angle: 360,
            x: this.phaserSprite.x + newXOffset * (this.counter % 2 == 1 ? 1 : -1),
            y: this.phaserSprite.y + newYOffset * (this.counter % 2 == 1 ? 1 : -1),
            scaleX: this.counter % 2 == 1 ? 1.5 : 0.3,
            scaleY: this.counter++ % 2 == 1 ? 1.5 : 0.3,
            duration: 200000,
            onComplete: () => this.animPhaserImage()
        });

        this.time.addEvent({
            delay: 2000,
            callback: () => (this.phaserSprite.tint = [0x123456, 0xfff091, 0xba34ac, 0xffaafa, 0x1266ff, 0x00f0ff][Math.floor(Math.random() * 6)]),
            repeat: -1
        });
    }
}
