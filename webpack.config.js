const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    entry: {
        game: './src/game.ts',
        phaser: ['phaser']
    },

    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js'
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },

    // module: {
    //   rules: [{
    //     test: /\.ts$/,
    //     include: path.resolve(__dirname, 'src/'),
    //     use: {
    //       loaders: ['babel-loader', 'ts-loader'],
    //       options: {
    //         presets: ['env']
    //       }
    //     }
    //   }]
    // },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },

    devServer: {
        contentBase: path.resolve(__dirname, 'build')
    },

    plugins: [
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'index.html'),
                to: path.resolve(__dirname, 'build')
            },
            {
                from: path.resolve(__dirname, 'assets', '**', '*'),
                to: path.resolve(__dirname, 'build')
            },
            {
                from: path.resolve(__dirname, 'css', '**', '*'),
                to: path.resolve(__dirname, 'build')
            },
            {
                from: path.resolve(__dirname, 'plugins', '**', '*'),
                to: path.resolve(__dirname, 'build')
            }
        ]),
        new webpack.DefinePlugin({
            'typeof CANVAS_RENDERER': JSON.stringify(true),
            'typeof WEBGL_RENDERER': JSON.stringify(true)
        })
    ]
};
